/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author watan
 */
public class TestUpdateProduct {
    public static void main(String[] args) {
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql ="UPDATE product SET name = ? ,price = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Product product = new Product(-1,"Latte",35);
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
            stmt.setInt(3, 7);
            int row = stmt.executeUpdate();
            System.out.println("Affect row "+ row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE,null,ex);
        }
        db.close();
    }
}
